import React from 'react'
import { withTranslation } from 'react-i18next';
import { connect } from 'react-redux'
import { Link } from "react-router-dom";
import { NavLink } from 'react-router-dom'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInstagram } from '@fortawesome/free-brands-svg-icons'



function mapStateToProps(store: {
	loginReducer: any,
	registerReducer: any,
	languageReducer: any,
	subscriptionReducer: any,
	paymentReducer: any,
}) {
	return {
		loginReducer: store.loginReducer,
		registerReducer: store.registerReducer,
		languageReducer: store.languageReducer,
		subscriptionReducer: store.subscriptionReducer,
		paymentReducer: store.paymentReducer,
	};
}

class Home extends React.Component<{}, {

}> {

	props: any
	static propTypes: any
	static defaultProps: any

	// eslint-disable-next-line no-useless-constructor
	constructor(props: any) {
		super(props);
		this.state = {

		};
	}



 

	render(){

		const { t } = this.props;


		return(
			<div>
				<header>
					<div className="container-fluid">
								<div className="row align-items-center">
									<div className="col-4">									
									<NavLink className="navbar-brand" to="/">rostro mostro</NavLink> 
								{/* <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
									<span className="navbar-toggler-icon"></span>
								</button> */}
									 </div>
									<div className="col-8 d-flex justify-content-end">

									<nav>
										<ul className="nav justify-content-end">
										<li className="nav-item">
										<NavLink className="nav-link" to="/rostromostro" activeClassName="active-link-nav">proyecto</NavLink>
										</li>
										<li className="nav-item">
										<NavLink className="nav-link" to="/audios" activeClassName="active-link-nav"> audios</NavLink>
										</li>
										<li className="nav-item">
										<a className="nav-link" href="https://www.instagram.com/rostro.mostro/" target="blank" ><FontAwesomeIcon icon={faInstagram} /> </a>
										</li>
										
										</ul>
									</nav>
											
												
											
								</div>
						</div>
					</div>
			

				{/* <div className="container-fluid">
					<div className="row">
						<div className="col-12">
							<nav>
							<ul className="nav justify-content-end">
										<li className="nav-item">
											<a className="nav-link active" href="#">Active</a>
										</li>
										<li className="nav-item">
											<a className="nav-link" href="#">Link</a>
										</li>
										<li className="nav-item">
											<a className="nav-link" href="#">Link</a>
										</li>
										<li className="nav-item">
											<a className="nav-link disabled" href="#">Disabled</a>
										</li>
										</ul>
							</nav>
						</div>
					</div>
				</div> */}
		
				</header>

		
			 
			</div>
		);
	}
}

const Export = withTranslation()(Home)

export default connect(mapStateToProps)(Export)