import React, { useEffect } from 'react'
import { withTranslation } from 'react-i18next';

import { Header } from '../Header';
import { Footer } from '../Footer';
import { Carousel } from '../Carousel';
import { Drag } from '../Drag';
import AOS from 'aos';
import './css/home.css'

AOS.init();

function ScrollToTopOnMount() {
	useEffect(() => {
		window.scrollTo(0, 0);
	}, []);
	return null;
}

class Login extends React.Component<{}, {
}> {

	render() {
		return (
			<div className="App">
				<ScrollToTopOnMount />
				<Header />
				<section className="home" id="home">
					{/* <video autoPlay loop muted className="video-hero"><source src={fondo} type="video/mp4"/></video> */}
					<div className="capa"></div>
					<div className="container-fluid">
						<div className="row row-height">
							<div className="col-12 col-md-4">
								<h1 data-aos="fade" data-aos-delay="300" data-aos-duration="900">¿Te animas a armar <br /> tu rostro monstruoso? <br />¿A escuchar sus demandas? </h1>
								<p data-aos="fade" data-aos-delay="1200" data-aos-duration="800" className="subtitle">Una invitación, un manifiesto, una articulación de distintas formas de lo humano. <br /> El rostro como una creación colectiva.</p>
							</div>
							<div className="col-12 col-md-8">
								<Drag />
								<p className="cartel d-none d-sm-none d-md-block" data-aos="fade-left" data-aos-delay="2600" data-aos-duration="800" >Jugá con los elementos para armar tu rostro.</p>
								<p className="cartel d-block d-sm-block d-md-none">Jugá con los elementos <br/> para armar tu rostro.</p>
							</div>
						</div>
						<div className="row">
							<div className="col-12 col-md-4">
								<Carousel />
							</div>
						</div>
					</div>
				</section>
				<Footer />
			</div>
		);
	}
}

const Export = withTranslation()(Login)

export default (Export)