import React from 'react';
import CarouselExport from './Carousel'
import Audio1Export from './Audio1'
import Audio2Export from './Audio2'
import Audio3Export from './Audio3'
import Audio4Export from './Audio4'
import Audio5Export from './Audio5'


export function Carousel(props: any) {

  return <CarouselExport {...props}/>; 
  
}

export function Audio1(props: any) {

  return <Audio1Export {...props}/>; 
  
}
export function Audio2(props: any) {

  return <Audio2Export {...props}/>; 
  
}
export function Audio3(props: any) {

  return <Audio3Export {...props}/>; 
  
}
export function Audio4(props: any) {

  return <Audio4Export {...props}/>; 
  
}
export function Audio5(props: any) {

  return <Audio5Export {...props}/>; 
  
}
