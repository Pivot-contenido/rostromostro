import React, { Component } from 'react'
import PlayCircleFilledIcon from '@material-ui/icons/PlayCircleFilled';
import PlayButton from 'simple-play-button'
import PauseCircleFilledIcon from '@material-ui/icons/PauseCircleFilled';

const audios = require("../assets/audio2.wav") 

class Audio1 extends React.Component  {
  render () {
    return (
    //   <PlayButton src={audios}/>
    <PlayButton src={audios} custom>
    {({playOrPause, isPlaying}) => (
      <div onClick={playOrPause}>
        {!isPlaying ? <div><i className="far fa-play-circle playicon"></i></div> : <div><i className="far fa-pause-circle pauseicon"></i></div>}
      </div>
    )}
  </PlayButton>
    )
  }
}



const Export = (Audio1)

export default (Export)