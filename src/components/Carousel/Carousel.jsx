import React, { Component } from 'react';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

import { Audio1 } from '.';
import { Audio2 } from '.';
import { Audio3 } from '.';
import { Audio4 } from '.';
import { Audio5 } from '.';
import './carousel.css';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';

export class Owldemo1 extends Component {
	state = {
		responsive: {
			0: {
				items: 1,
			},
			450: {
				items: 1,
			},
			600: {
				items: 1,
			},
			1000: {
				items: 1,
			},
		},
	}


	render() {
		return (
			<div>
				<OwlCarousel
					className="owl-theme"
					loop="true"
					autoplay="true"
					autoplayHoverPause="true"
					responsiveClass="true"
					nav
					navContainerClass="owl-navs"
					dotsContainer="false"
					responsive={this.state.responsive}
					margin={8} >
					<div>
						<h4>¿Te preguntaste hasta qué punto sos<br />eso que venís siendo?</h4>
						<Audio1 />
					</div>
					<div>
						<h4>Monstruos <br />pero también mariposas</h4>
						<Audio2 />
					</div>
					<div>
						<h4>No nos incluyas,<br />queremos ser otra cosa</h4>
						<Audio3 />
					</div>
					<div>
						<h4>Nos sostiene <br />una red de monstruos</h4>
						<Audio4 />
					</div>
					<div>
						<h4>Si seguimos abriendo casillas,<br />¿estallará el tablero?</h4>
						<Audio5 />
					</div>
				</OwlCarousel>
			</div>
		)
	}
}

export default Owldemo1