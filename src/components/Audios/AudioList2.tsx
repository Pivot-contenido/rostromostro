import React from 'react';
import {AudioPlayerControlSprite, Audio, AudioPlayer, TrackType} from 'react-audio-player-pro';
import 'react-audio-player-pro/dist/style.css';
import './audio-list.css';


const audioTrackList: Array<TrackType> = [
    {
        // string - path to audio file, required
        src:'http://www.rostromostro.com.ar/static/media/audio1.dc1c40f0.wav',

        // React$Node - custom content instead of title, optional, deafult: <title> or <src>
        // content: <CustomContent/>,

        // MediaMetadata - media meta data, see `mediaMetadata` above
        // https://developer.mozilla.org/en-US/docs/Web/API/MediaMetadata/MediaMetadata
        // optional
        mediaMetadata: {
            title: 'Te preguntaste hasta qué punto sos eso que venis siendo',
            // artist: 'J. Syreus Bach',
        },

    },
    {
        // string - path to audio file, required
        src: 'http://www.rostromostro.com.ar/static/media/audio2.1b9bbdf0.wav',

        // React$Node - custom content instead of title, optional, deafult: <title> or <src>
        // content: <CustomContent/>,

        // MediaMetadata - media meta data, see `mediaMetadata` above
        // https://developer.mozilla.org/en-US/docs/Web/API/MediaMetadata/MediaMetadata
        // optional
        mediaMetadata: {
            title: 'Monstruos pero tambien mariposas',
        },
    },
    {
        // string - path to audio file, required
        src: 'http://www.rostromostro.com.ar/static/media/audio3.a01b2e3a.wav',

        // React$Node - custom content instead of title, optional, deafult: <title> or <src>
        // content: <CustomContent/>,

        // MediaMetadata - media meta data, see `mediaMetadata` above
        // https://developer.mozilla.org/en-US/docs/Web/API/MediaMetadata/MediaMetadata
        // optional
        mediaMetadata: {
            title: 'No nos incluyas, queremos ser otra cosa',
        },

    },
    {
        // string - path to audio file, required
        src: 'http://www.rostromostro.com.ar/static/media/audio4.671f5be9.wav',

        // React$Node - custom content instead of title, optional, deafult: <title> or <src>
        // content: <CustomContent/>,

        // MediaMetadata - media meta data, see `mediaMetadata` above
        // https://developer.mozilla.org/en-US/docs/Web/API/MediaMetadata/MediaMetadata
        // optional
        mediaMetadata: {
            title: 'Nos sostiene una red de monstruos',
        },
    },
    {
        // string - path to audio file, required
        src: 'http://www.rostromostro.com.ar/static/media/audio5.b5d31b75.wav',

        // React$Node - custom content instead of title, optional, deafult: <title> or <src>
        // content: <CustomContent/>,

        // MediaMetadata - media meta data, see `mediaMetadata` above
        // https://developer.mozilla.org/en-US/docs/Web/API/MediaMetadata/MediaMetadata
        // optional
        mediaMetadata: {
            title: 'Si seguimos abriendo casillas ¿Estallará el tablero?',
        },
    },
    // other tracks here...
];

export function ExampleAudioPlayer() {
    return (
        <>
            <AudioPlayerControlSprite/>
            <AudioPlayer
                // Array<TrackType> - list of track, see `audioTrackList` above, required
                trackList={audioTrackList}

                // string - wrapper's class name, optional, deafult: ''
                className=""
                
                // callback function - called on did mount, optional, default: noop
                onDidMount={console.log}

                // default player state, optional
                defaultState={{
                    // boolean - is player muted, optional, default: false
                    isMuted: false,			
	

                    // number - active song index, optional, default: 0
                    activeIndex: 0,

                    // boolean - is shuffle on, optional, default: false
                    isShuffleOn: false,

                    // boolean - is track list open, optional, default: true
                    isTrackListOpen: true,

                    // string: 'none' | 'all' | 'one' - repeating state, optional, default: 'none'
                    repeatingState: 'none',
                }}
            />

        </>
    );
}