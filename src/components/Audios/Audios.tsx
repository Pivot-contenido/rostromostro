import React, { useEffect } from 'react'
import { Header } from '../Header';
import { Footer } from '../Footer';
import { ExampleAudioPlayer } from '../Audios';

const audio1 = require("../assets/audio1.wav") 
const audio2 = require("../assets/audio2.wav") 
const audio3 = require("../assets/audio3.wav") 
const audio4 = require("../assets/audio4.wav") 
const audio5 = require("../assets/audio5.wav") 

function ScrollToTopOnMount() {
	useEffect(() => {
		window.scrollTo(0, 0);
	}, []);

	return null;
}

class Home extends React.Component<{}, {

}> {

	render() {

		return (
			<div>
				<ScrollToTopOnMount />
				<Header />
				<section className="audios">
					<div className="container">
						<div className="row">
							<div className="col-12 col-xl-12">
								<ExampleAudioPlayer />
							</div>
							<div className="col-12 col-xl-12">
								<p>
									<a className="btn btn-primary yellow-buttons mt-4" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
									Descargar audios</a>
								</p>
								<div className="collapse" id="collapseExample">
									<div className="card card-body">
										<a href={ audio1 } download="Te preguntaste hasta qué punto sos eso que venis siendo"  target="_blank" title="Te preguntaste hasta qué punto sos eso que venis siendo">Te preguntaste hasta qué punto sos eso que venis siendo</a>
										<a href={ audio2 }  download="Monstruos pero tambien mariposas" target="_blank" title="Monstruos pero tambien mariposas">Monstruos pero tambien mariposas</a>
										<a href={ audio3 }  download="No nos incluyas, queremos ser otra cosa" target="_blank" title="No nos incluyas, queremos ser otra cosa">No nos incluyas, queremos ser otra cosa</a>
										<a href={ audio4 }  download="Nos sostiene una red de monstruos" target="_blank" title="Nos sostiene una red de monstruos">Nos sostiene una red de monstruos</a>
										<a href={ audio5 }  download="Si seguimos abriendo casillas ¿Estallará el tablero?" target="_blank" title="Si seguimos abriendo casillas ¿Estallará el tablero?">Si seguimos abriendo casillas ¿Estallará el tablero?</a>

										</div>
								</div>
							</div>
						</div>
					</div>
				</section>



				<Footer />
			</div>
		);
	}
}

const Export = (Home)

export default (Export)