import TextLoop from "react-text-loop";

import React, { Component } from "react";
 
class App extends Component {
    render() {
        return (
            <h6>
                <TextLoop>
                    <span>Los monstruos están <br/> entre nosotres.</span>
                    <span>Los monstruos <br/> estamos.</span>

                </TextLoop>
            </h6>
      
        );
    }
}

export default App