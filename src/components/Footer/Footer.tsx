import React from 'react'
import { withTranslation } from 'react-i18next';
import { connect } from 'react-redux'


import { FooterText } from '..//Footer';

import licencia from './img/licencia.png';

import './css/PaymentList.css'




function mapStateToProps(store: {


}) {
	return {
		

	};
}

class Home extends React.Component<{}, {
	classSidebar: string,
	classButton: string,
}> {

	props: any
	static propTypes: any
	static defaultProps: any

	// eslint-disable-next-line no-useless-constructor
	constructor(props: any) {
		super(props);
		this.state = {
			classSidebar: '',
			classButton: 'navbar-btn',
		};
	}



	render(){

		return(
			<div>
				<footer>
					<div className="container">
						<div className="row">
							<div className="col-12 col-md-6 mt-4">
								{/* <h6>Los monstruos están <br/>
		entre nosotres,<br/>
		los monstruos estamos.</h6> */}
								<FooterText />
							</div>
							<div className="col-12 col-md-6 d-flex justify-content-md-end mt-4">
								<div>
							<img src={licencia} alt="licencia cc" />
							<p className="licencia">Atribución-NoComercial-CompartirIgual <br/>CC BY-NC-SA</p>
							</div>
							</div>
						</div>
					</div>
				</footer>

		
			 
			</div>
		);
	}
}

const Export = withTranslation()(Home)

export default connect(mapStateToProps)(Export)