import React, { useEffect } from 'react'



import { Header } from '../Header';
import { Footer } from '../Footer';

const rostroImg = require("../assets/rostro.png")


function ScrollToTopOnMount() {
	useEffect(() => {
		window.scrollTo(0, 0);
	}, []);

	return null;
}



class Home extends React.Component<{}, {
}> {


	render() {
		return (
			<div>
				<ScrollToTopOnMount />
				<Header />
				<section className="about">
					<div className="container">
						<div className="row">
							<div className="col-12 col-xl-6">
								<h2>rostro mostro</h2>
								<p>
									<b>Rostro Mostro</b> es un proyecto de <b>creación colectiva</b> que busca abrir espacios a<b> nuevas formas de existencia, </b>
									generar un encuentro en la jungla de la virtualidad que nos permita respirar entre los resquicios del sistema binario. </p>
								<p>
									<b>Sabemos que somos muchxs, que no estamos solxs.</b> Que este mundo está fundado sobre nuestros cuerpos, nuestros rostros, nuestros rasgos monstruosos. Emergemos una vez más para invitarte a mirarnos a los ojos y descubrir tu reflejo. </p>

								<p>A este rostro lo conformamos a través de una convocatoria virtual.<b> El agradecimiento es para Guadalupe Aráoz, Kryptonita Amapola, Agustina Oberti, Lucia, Sabrina C., Chapu Carpentier, Ludmi y Lavero, Antto, Simón, Julz Donzelli, Macarena Muru, *gr.zia. </b></p>

								<p>Este proyecto se considera en permantente construcción y no pretende ser un producto acabado. Si quieren dialogar con nosotres, 
							aportar ideas o enviarnos la captura de su rostro monstruoso pueden hacerlo a <a href="mailto:losrostrosdelamonstruosidad@gmail.com">losrostrosdelamonstruosidad@gmail.com </a> o
							<a target="blank" href="https://www.instagram.com/rostro.mostro/"> @rostromostro. </a> 

									</p>
								<p>Rostro Mostro es parte de una tesis de grado de la Facultad de Ciencias de la Comunicación - UNC.
									</p>
							</div>
							<div className="col-12 col-xl-6 d-flex justify-content-center align-items-center">


								<img src={rostroImg} className="rostro-gif" alt="licencia cc" />

							</div>

						</div>
					</div>

				</section>



				<Footer />
			</div>
		);
	}
}

const Export = (Home)

export default (Export)