// App.js
import React from 'react';
import './css/drag.css';

import Draggable from 'react-draggable';

const img1 = require("../assets/narizyboca.png")
const img2 = require("../assets/narizyboca2.png")
const img3 = require("../assets/ojo1.png")
const img4 = require("../assets/ojo2.png")
const img5 = require("../assets/ojo3.png")
const img6 = require("../assets/ojo4.png")
const img7 = require("../assets/ojo5.png")
const img8 = require("../assets/ojo6.png")
const img9 = require("../assets/ojo7.png")
const img10 = require("../assets/ojo8.png")
const img11 = require("../assets/ojo9.png")
const img12 = require("../assets/ojo10.png")
const img13 = require("../assets/boca1.png")
const img14 = require("../assets/boca2.png")
const img15 = require("../assets/boca3.png")
const img16 = require("../assets/boca4.png")
const img17 = require("../assets/boca5.png")
const img18 = require("../assets/boca6.png")
const img19 = require("../assets/boca7.png")
const img20 = require("../assets/boca8.png")
const img29 = require("../assets/boca9.png")
const img21 = require("../assets/frente1.png")
const img22 = require("../assets/frente2.png")
const img23 = require("../assets/frente3.png")
const img24 = require("../assets/frente4.png")
const img25 = require("../assets/frente5.png")
const img26 = require("../assets/nariz1.png")
const img27 = require("../assets/nariz2.png")
const img28 = require("../assets/nariz3.png")



class App extends React.Component {

    

    eventControl = (event: { type: any; }, info: any) => {
        console.log('Event name: ', event.type);
        console.log(event, info);
    }

    render() {
        return (
            <div className="drag-zone">
             


                        
                            <>
                                <Draggable
                                defaultPosition={{x: 120, y: 230}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img21} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                 defaultPosition={{x: 180, y: -120}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img23} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                    defaultPosition={{x: -200, y: -150}}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img22} /></div>
                                    </div>
                                </Draggable>

                                <Draggable
                                 defaultPosition={{x: 100, y: 0}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img24} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                  defaultPosition={{x: 100, y: 50}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img25} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                  defaultPosition={{x: 150, y: 80}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img29} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                 defaultPosition={{x: -260, y: 90}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img1} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img2} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img3} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img4} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                 defaultPosition={{x: 285, y: 20}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img5} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                 defaultPosition={{x: -130, y: -10}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img6} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                 defaultPosition={{x: 140, y: -75}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img7} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                 defaultPosition={{x: -160, y: 220}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img8} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                 defaultPosition={{x: -104, y: 108}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img9} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                  defaultPosition={{x: -240, y: -80}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img10} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                 defaultPosition={{x: 220, y: -50}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img11} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                 defaultPosition={{x: -70, y: -210}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper d-none d-sm-none d-md-block">
                                        <div><img src={img12} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                 defaultPosition={{x: 0, y: 230}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img13} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                   defaultPosition={{x: -210, y: 170}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img14} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                 defaultPosition={{x: 20, y: -160}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img15} /></div>
                                    </div>
                                </Draggable>
                                <Draggable

                                 defaultPosition={{x: -230, y: -70}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img16} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                 defaultPosition={{x: 200, y: 150}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img17} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                 defaultPosition={{x: -105, y: -50}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img18} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img19} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                 defaultPosition={{x: 100, y: 50}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img20} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img26} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                 defaultPosition={{x: -130, y: -70}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img27} /></div>
                                    </div>
                                </Draggable>
                                <Draggable
                                 defaultPosition={{x: 0, y: 150}}
                                    onDrag={this.eventControl}
                                    onStart={this.eventControl}
                                    onStop={this.eventControl}
                                >
                                    <div className="drag-wrapper">
                                        <div><img src={img28} /></div>
                                    </div>
                                </Draggable>
                            </>
                        </div>
           
        );
    }
}

export default App;