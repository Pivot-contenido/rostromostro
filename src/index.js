import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './i18n';
import Router from './Router'
import { Provider } from 'react-redux';
import store from './store/index';

ReactDOM.render(
  <Suspense fallback=''>
    <Provider store={store}>
      <React.StrictMode>
        <Router />
      </React.StrictMode>
    </Provider>
  </Suspense>,
  document.getElementById('root')
);

