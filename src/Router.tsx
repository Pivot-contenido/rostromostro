import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";
import Cookies from 'universal-cookie';

import { RostroMostro } from './components/RostroMostro'
import { Home } from './components/Home'
import { Footer } from './components/Footer'
import { Audios } from './components/Audios'
import { Drag } from './components/Drag'





class App extends React.Component <{}, {}> {

	constructor(props: any) {
		super(props)
	}

	render() {

		const cookies = new Cookies();

		return (
			<Router>
				<div>
					<Switch>					
			
						<Route component={ RostroMostro } path='/rostromostro' />
						<Route component={ Footer } path='/footer' />
						<Route component={ Audios } path='/audios' />			
						<Route component={ Drag } path='/drag' />
						<Route component={ Home } path='/' />	
					
					</Switch>
				</div>
			</Router>
		);
	}
}

export default App